<?php

use Faker\Generator as Faker;

$factory->define(App\modelo\Language::class, function (Faker $faker) {
    return [
        'label'=>$faker->word,
        'iso6391'=>$faker->languageCode,

    ];
});
