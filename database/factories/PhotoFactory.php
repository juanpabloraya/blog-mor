<?php

use Faker\Generator as Faker;

$factory->define(App\modelo\Photo::class, function (Faker $faker) {
    return [
    	'filename' => $faker->image('public/imagenes',100,100,null,false),
    	'type' => 'jpg', 
        
    ];
});
