<?php

use Illuminate\Database\Seeder;
use App\modelo\Photo;

class PhotosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Photo::class, 10)->create();
    }
}
