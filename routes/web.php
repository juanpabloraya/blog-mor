<?php
use App\modelo\User;
use App\modelo\Photo;
use App\modelo\Language;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('usuarios', function () {
    return User::get();
});
Route::get('photos', function () {
    return Photo::get();
});
Route::get('languages', function () {
    return Language::get();
});

